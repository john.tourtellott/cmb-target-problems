CMB target problem 3a: solidification
--------------------------------------------------------
* 2 materials: single-phase and 2-phase
* constant material properties
* flux, htc, radiation boundary conditions (no dirichlet)
* htc, radiation interface conditions

&OUTPUTS
  output_t = 0.0, 120.0, 600.0
  output_dt = 10.0, 60.0
/

&MESH
  mesh_file = 'mesh2.gen'
  coordinate_scale_factor = 0.001
  interface_side_sets = 24, 30, 40, 41, 42, 50, 51
/

&PHYSICS
  heat_transport = .true.
/

&DIFFUSION_SOLVER
  abs_temp_tol = 0.0
  rel_temp_tol = 1.0e-3
  abs_enthalpy_tol = 0.0
  rel_enthalpy_tol = 1.0e-3
  max_nlk_itr = 5
  nlk_tol = 0.02
  nlk_preconditioner = 'hypre_amg'
  pc_amg_cycles = 2
  verbose_stepping = .true.
/

&NUMERICS
  dt_init = 0.01
  dt_grow = 5.0
  dt_max = 60.0
/

### BODIES #####################################################################

# Inner mold - graphite
&BODY
  surface_name = 'from mesh file'
  mesh_material_number = 2
  material_number = 1
  temperature_function = 'initial-temp'
/

# Outer mold - graphite
&BODY
  surface_name = 'from mesh file'
  mesh_material_number = 3
  material_number = 1
  temperature_function = 'initial-temp'
/

# Funnel - graphite
&BODY
  surface_name = 'from mesh file'
  mesh_material_number = 4
  material_number = 1
  temperature_function = 'initial-temp'
/

# Base - graphite
&BODY
  surface_name = 'from mesh file'
  mesh_material_number = 5
  material_number = 1
  temperature_function = 'initial-temp'
/

Linear temperature profile in the vertical (z) coordinate from
T =  at the bottom to T=1400 at the top
&FUNCTION
  name = 'initial-temp'
  type = 'tabular'
  tabular_data = -0.075, 1200.0
                  0.285, 1400.0
  tabular_dim = 3
/

# Cast uranium
&BODY
  surface_name = 'from mesh file'
  mesh_material_number = 1
  material_number = 3
  temperature = 1500
/

### BOUNDARY CONDITIONS #######################################################

&DS_BOUNDARY_CONDITION
  name = 'symmetry'
  variable = 'temperature'
  condition = 'flux'
  face_set_ids = 1
  data_constant = 0.0
/

&DS_BOUNDARY_CONDITION
  name = 'base-bottom'
  variable = 'temperature'
  face_set_ids = 2
  condition = 'HTC'
  data_constant = 50.0, 300.0
/

&DS_BOUNDARY_CONDITION
 name = 'outer'
 variable = 'temperature'
 face_set_ids = 20, 21, 22
 condition = 'radiation'
 data_constant = 0.75, 300.0
/

&DS_BOUNDARY_CONDITION
  name = 'inner'
  variable = 'temperature'
  face_set_ids = 6, 10, 11, 23 
  condition = 'flux'
  data_constant = 0.0
/

### INTERFACE CONDITIONS #######################################################

&DS_INTERFACE_CONDITION
  name = 'base-core'
  variable = 'temperature'
  face_set_ids = 30
  condition = 'HTC'
  data_constant = 500.0
/

&DS_INTERFACE_CONDITION
  name = 'core-case-gap'
  variable = 'temperature'
  face_set_ids = 40
  condition = 'radiation'
  data_constant = 0.75
/

&DS_INTERFACE_CONDITION
  name = 'core-case'
  variable = 'temperature'
  face_set_ids = 41, 42
  condition = 'HTC'
  data_constant = 500.0
/

&DS_INTERFACE_CONDITION
  name = 'funnel-case'
  variable = 'temperature'
  face_set_ids = 50, 51
  condition = 'HTC'
  data_constant = 200.0
/

&DS_INTERFACE_CONDITION
  name = 'mold-casting'
  variable = 'temperature'
  face_set_ids = 24
  condition = 'HTC'
  data_constant = 500.0
/

### MATERIALS ##################################################################

### 2020 GRADE GRAPHITE ###

&MATERIAL_SYSTEM
  name = 'graphite-2020'
  phases = 'graphite-2020'
/

&MATERIAL
  material_number = 1
  material_name = 'graphite-2020'
  density = 1.0 ! not void -- value not significant
  material_feature = 'background'
  immobile = .true.
/

&PHASE
  name = 'graphite-2020'
  property_name(1) = 'density',       property_constant(1) = 1750.0
  property_name(2) = 'specific heat', property_constant(2) = 1500.0
  property_name(3) = 'conductivity',  property_constant(3) = 100.0
/

### URANIUM ###

&MATERIAL_SYSTEM
  name = 'uranium'
  phases = 'gamma-U', 'liquid-U'
  transition_temps_low  = 1396.0
  transition_temps_high = 1416.0
  smoothing_radius = 10.0
  latent_heat = 35794
/

&PHASE
  name = 'gamma-U'
  property_name(1) = 'density',       property_constant(1) = 17.5e3
  property_name(2) = 'specific heat', property_constant(2) = 160.9
  property_name(3) = 'conductivity',  property_constant(3) = 46.05
/

&MATERIAL
  material_number = 2
  material_name = 'gamma-U'
  density = 1.0 ! not void -- value not significant
  immobile = .true.
/

&PHASE
  name = 'liquid-U'
  property_name(1) = 'density',       property_constant(1) = 17.5e3
  property_name(2) = 'specific heat', property_constant(2) = 201.3
  property_name(3) = 'conductivity',  property_constant(3) = 46.05
  property_name(4) = 'viscosity',     property_constant(4) = 5.00e-3
/

&MATERIAL
  material_number = 3
  material_name = 'liquid-U'
  density = 1.0 ! not void -- value not significant
/
